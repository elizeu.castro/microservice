from django.test import TestCase

# Create your tests here.
from exemplo.views import opa


class ExemploTest(TestCase):
    def test_opa_maior_que_zero(self):
        # given
        numero = 1
        expected_payload = '1'
        # when
        response = opa(numero)
        # then
        self.assertEqual(expected_payload, response["hey"])

    def test_opa_menor_que_zero(self):
        # given
        numero = -1
        expected_payload = 'negativo'
        # when
        response = opa(numero)
        # then
        self.assertEqual(expected_payload, response["hey"])

    def test_opa_igual_a_zero(self):
        # given
        numero = 0
        expected_payload = "let's go"
        # when
        response = opa(numero)
        # then
        self.assertEqual(expected_payload, response["ho"])
